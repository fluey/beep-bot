const Discord = require("discord.js");
const client  = new Discord.Client();

const config = require("./config.json");

const BOT_VERSION = "1.0.0";

var newUsers = [];

client.on("ready", () => {
    console.log("Bot started! Version " + BOT_VERSION);
});

client.on("guildMemberAdd", (member) => {
    newUsers.push(member.user.id);
	console.log(member.user.username + " has joined the server.");
	if (newUsers.length > config.user_threshold) {
		member.guild.channels.find("name", "default").send("Welcome newbies, please be sure to read the <#" + member.guild.channels.find("name", config.mentioned_channel).id + "> channel.");
		newUsers = [];
	}
});

client.login(config.token);