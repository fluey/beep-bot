Setup:
Fill out config.json with the desired configs and a valid discord token.

Use this tutorial to create a bot for your account and get the token:
https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token

Run:
Change directory to this project and run `npm install`
There might be some minor errors with sodium, but it should work.

Once that has finish, use `node main.js` and the bot should automatically connect to the approved channels.